<?php

return [
    'sandbox' => env('PLUG_NOTAS_SANDBOX',false),
    'token_key' => env('PLUG_NOTAS_AUTH_TOKEN'),
    'token_key_sandbox' => env('PLUG_NOTAS_AUTH_TOKEN_SANDBOX')
];

