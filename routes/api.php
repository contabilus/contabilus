<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\AuthApiController;
use App\Http\Controllers\Api\Certificate\CertificateApiController;
use App\Http\Controllers\Api\Company\CompanyApiController;
use App\Http\Controllers\Api\Service\ServiceApiController;
use App\Http\Controllers\Api\Taker\TakerApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthApiController::class, 'register']);
Route::post('/auth', [AuthApiController::class, 'login']);
Route::post('/forgot-password', [AuthApiController::class, 'forgotPassword']);
Route::post('/reset-password', [AuthApiController::class, 'resetPassword']);
Route::apiResource('/taker', TakerApiController::class);
Route::apiResource('/service', ServiceApiController::class);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::apiResource('/certificate', CertificateApiController::class)->except('destroy');
    Route::get('/me', [AuthApiController::class, 'getMe']);
    Route::post('/logout', [AuthApiController::class, 'logout']);
});

Route::group(['middleware' => 'auth:sanctum', 'prefix' => 'company'], function () {
    Route::apiResource('/', CompanyApiController::class);
    Route::get('/cnpj/{cnpj}', [CompanyApiController::class, 'findCnpj']);
});
