<?php

namespace App\Helpers\CodeHelper;

class CodeHelper
{
    public static function genereateRandomCode(int $length)
    {
        return strval(rand(self::repeatValue(1,$length), self::repeatValue(9,$length)));
    }

    private static function repeatValue($initialValue, $repetitions)
    {
        if($initialValue == "1") {
            return "1".str_repeat('0', $repetitions - 1 );
        }
        return str_repeat($initialValue, $repetitions);
    }
}
