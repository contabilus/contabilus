<?php

namespace App\Enums\Companies;

class REGIME_ENUM
{


    const NONE_REGIME = 0;
    const SN_REGIME = 1;
    const SN_EXCEED_REGIME = 2;
    const LP_REGIME = 3;
    const LR_REGIME = 4;

    const TAX_REGIME = [
        self::NONE_REGIME,
        self::SN_REGIME,
        self::SN_EXCEED_REGIME,
        self::LP_REGIME,
        self::LR_REGIME,
    ];
}
