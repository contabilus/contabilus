<?php

namespace App\Enums\Companies;

class TAX_REGIME_ENUM
{


    const NONE_REGIME = 0;
    const MEM_REGIME = 1;
    const ESTIMATE_REGIME = 2;
    const PROFESSIONALS_SOCIETY_REGIME = 3;
    const COOPERATIVE_REGIME = 4;
    const MEI_REGIME = 5;
    const ME_EPP_REGIME = 6;

    const TAX_REGIME = [
      self::NONE_REGIME,
      self::MEM_REGIME,
      self::ESTIMATE_REGIME,
      self::PROFESSIONALS_SOCIETY_REGIME,
      self::COOPERATIVE_REGIME,
      self::MEI_REGIME,
      self::ME_EPP_REGIME,
    ];
}
