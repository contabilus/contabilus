<?php

namespace App\Services\NFE\Classes;

use App\Services\NFE\Abstracts\ApiClient;
use GuzzleHttp\Client;

class Endpoints extends ApiClient
{
    private $methods;
    private $endpoints;
    private $collection;

    public function __construct($collection, $options, $client = null )
    {
        $this->client = $client;
        if ( !$this->client ) {
            parent::__construct($options);
        }

        $this->endpoints = Config::get('ENDPOINTS');

        $this->collection = $collection;
        $this->map();
    }

    public function __call($method, $args)
    {
        if(isset($this->methods[$method])){
            return $this->methods[$method]($args[0] ?? [], $args[1] ?? [] );
        } else {
            throw new \Exception('Endpoint don\'t exist');
        }
    }

    private function map()
    {
        $this->methods = array_map(function($endpoint) {
            return function ($params = [], $body = []) use ($endpoint) {
                $route = $this->getRoute($endpoint, $params);
                $query = $this->getQueryParams($params);
                $route .= $query;
                return $this->send($endpoint['method'],$route, $body);
            };
        },$this->endpoints[$this->collection]);

    }

    private function getRoute($endpoint,&$params)
    {
        $route = $endpoint['route'];
        preg_match_all('/\:(\w+)/im',$route, $matches);
        $variables = $matches[1];

        foreach($variables as $value) {
            if(isset($params[$value])){
                $route = str_replace(':'.$value, $params[$value], $route);
                unset($params[$value]);
            }
        }
        return $route;
    }

    private function getQueryParams($params) {
        $keys = array_keys($params);
        $lenght = count($params);

        if($lenght==0) {
            return '';
        }

        $reduce = function ($previous,$current) use ($keys, $params, $lenght) {
            $next = ($current == $lenght -1) ? '': '&';

            return $previous.$keys[$current].'='.$params[$keys[$current]].$next;
        };

        return array_reduce(array_keys($keys),$reduce,'?');
    }

}
