<?php

namespace App\Services\NFE\Classes;

class Config
{
    private static $endpointFile = '../config.json';

    public static function setEndpointFile($file)
    {
        self::$endpointFile = $file;
    }

    public static function get($property)
    {
        $file = file_get_contents(self::$endpointFile);
        $config = json_decode($file,true);

        if(json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Erro ao carregar endpoints do arquivo');
        }

        if(isset($config[$property])) {
            return $config[$property];
        }

        return false;
    }

    public static function options($options)
    {
        $conf = [];

        if(isset($options['url'])) {
            $conf['baseUrl'] = $options['url'];
        } else {
            $url = self::get('URL')['DEFAULT'];
            $conf['baseUrl'] = config('plugnotas.sandbox') ? $url['sandbox']:$url['production'];
        }


        if(isset($options['headers'])){
            $conf['headers'] = $options['headers'];
        }

        return $conf;
    }
}
