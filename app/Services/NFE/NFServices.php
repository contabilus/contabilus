<?php

namespace App\Services\NFE;

use App\Services\NFE\Classes\Config;
use App\Services\NFE\Classes\Endpoints;

class NFServices extends Endpoints
{
    public function __construct($collection,$options= [] )
    {
        Config::setEndpointFile(base_path('app/Services/NFE/config.json'));
        parent::__construct($collection,$options);
    }
}
