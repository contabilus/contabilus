<?php

namespace App\Services\NFE\Abstracts;

use App\Services\NFE\Classes\Config;
use App\Services\NFE\Contracts\ApiClientContract;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

abstract class ApiClient implements ApiClientContract
{
    private $client;
    private $config;

    public function __construct(array $options = null)
    {
        $this->config = Config::options($options);

        $clientData = [
              'base_uri' => $this->config['baseUrl'],
              'timeout' => 30,
              'headers' => [
                  'X-API-KEY' => config('plugnotas.sandbox') ? config('plugnotas.token_key_sandbox'):config('plugnotas.token_key')
              ]
        ];

        $this->client = new Client($clientData);
    }

    public function send($method, $route, $options)
    {

        if(isset($this->config['headers'])) {
            foreach($this->config['headers'] as $key => $value) {
                $options[$key] = $value;
            }
        }
        try {
            $response = $this->client->request($method,$route,$options);
            return json_decode($response->getBody(),true);
        } catch (\Exception $e) {
            $response = ($e->getResponse()->getBody(true)) ? $e->getResponse()->getBody(true) : [];
            return json_decode($response, true);
        } catch (GuzzleException $e) {
            return $e->getMessage();
        }
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
}
