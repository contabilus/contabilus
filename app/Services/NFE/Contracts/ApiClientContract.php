<?php

namespace App\Services\NFE\Contracts;

interface ApiClientContract
{
   public function send($method, $route,$options);

}
