<?php

namespace App\Actions\Company;

use App\Models\Company;


class CreateCompany
{
    public function handle($payload)
    {

            $company = Company::create($payload);
            $company->address()->create($payload['address']);

        return $company;
    }
}
