<?php

namespace App\Actions\Company;

use App\Models\Company;


class UpdateCompany
{
    public function handle($payload, Company $company)
    {

        $company->update($payload);
        $company->address->update($payload['address']);
        return $company;
    }
}
