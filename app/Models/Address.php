<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'addressable_id',
        'addressable_type',
        'zipcode',
        'public_place',
        'street',
        'number',
        'complement',
        'neighborhood_type',
        'neighborhood',
        'city',
        'state',
        'country_code',
        'country'
    ];

    public function addressable()
    {
        return $this->morphTo();
    }
}
