<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    /** Relationship */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cnae()
    {
        return $this->belongsTo(Cnae::class);
    }

    protected $fillable = [
        'user_id',
        'cnae_id',
        'service_code',
        'service_details',
        'tax_code',
        'city_code',
        'city',
        'unit',
        'quantity',
        'others_tax',
        'taxable',
        'values',
        'iss',
        'pis',
        'cofins',
        'csll',
        'inss',
        'irrf',
        'cpp'
    ];

    protected $casts = [
        'values' => 'json',
        'iss' => 'json',
        'pis' => 'json',
        'cofins' => 'json',
        'csll' => 'json',
        'inss' => 'json',
        'irrf' => 'json',
        'cpp' => 'json'
    ];
}
