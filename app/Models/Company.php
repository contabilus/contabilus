<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public function scopeFromUser($query)
    {
        return $query->where('user_id', auth()->id());
    }
}
