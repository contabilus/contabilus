<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;

class RecoverToken extends Model
{
    use HasFactory;
    use Prunable;

    protected $fillable = [
        'email',
        'code',
        'timeout_token'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($token) {
            $token->timeout_token = Carbon::now()->addMinutes(10);
        });
    }

    public function prunable()
    {
        return static::where('timeout_token','>=',Carbon::now());
    }
}
