<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taker extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'corporate_name',
        'trading_name',
        'cpf_cnpj',
        'state_registration',
        'municipal_registration',
        'email',
        'public_agency',
        'phone_prefix',
        'phone_number',
        'address'
    ];

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }
}
