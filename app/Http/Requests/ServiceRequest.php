<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validate = match (strtolower($this->method())) {
            'post' => [
                'user_id' => 'required|int',
                'cnae_id' => 'required|int',
                // 'user_id' => 'required|int|exists:\App\Models\User,id',
                // 'cnae_id' => 'required|int|exists:\App\Models\Cnae,id',
                'service_code' => 'required|string',
                'service_details' => 'required|string',
                'tax_code' => 'required|string',
                'city_code' => 'required|int',
                'city' => 'required|string',
                'unit' => 'required|int',
                'quantity' => 'required|int',
                'others_tax' => 'required|numeric',
                'taxable' => 'required|bool',
                'values' => 'required|array',
                'values.baseCalculo' => 'required|numeric',
                'values.deducoes' => 'required|numeric',
                'values.descontoCondicionado' => 'required|numeric',
                'values.descontoIncondicionado' => 'required|numeric',
                'values.liquido' => 'required|numeric',
                'values.unitario' => 'required|numeric',
                'values.valorAproximadoTributos' => 'required|numeric',
                'iss' => 'required|array',
                'iss.tributavel' => 'required|bool',
                'iss.tipoTributacao' => 'required|string',
                'iss.exigibilidade' => 'required|bool',
                'iss.aliquota' => 'required|numeric',
                'iss.valor' => 'required|numeric',
                'iss.valorRetido' => 'required|numeric',
                'iss.processoSuspensao' => 'required|numeric',
                'pis' => 'required|array',
                'pis.baseCalculo' => 'required|numeric',
                'pis.aliquota' => 'required|numeric',
                'pis.valor' => 'required|numeric',
                'pis.cst' => 'required|int',
                'cofins' => 'required|array',
                'cofins.baseCalculo' => 'required|numeric',
                'cofins.aliquota' => 'required|numeric',
                'cofins.valor' => 'required|numeric',
                'cofins.cst' => 'required|int',
                'csll' => 'required|array',
                'csll.aliquota' => 'required|numeric',
                'csll.valor' => 'required|numeric',
                'inss' => 'required|array',
                'inss.aliquota' => 'required|numeric',
                'inss.valor' => 'required|numeric',
                'irrf' => 'required|array',
                'irrf.aliquota' => 'required|numeric',
                'irrf.valor' => 'required|numeric',
                'cpp' => 'required|array',
                'cpp.aliquota' => 'required|numeric',
                'cpp.valor' => 'required|numeric'
            ],
            'put' => [
                'user_id' => 'int',
                'cnae_id' => 'int',
                'service_code' => 'string',
                'service_details' => 'string',
                'tax_code' => 'string',
                'city_code' => 'int',
                'city' => 'string',
                'unit' => 'int',
                'quantity' => 'int',
                'others_tax' => 'numeric',
                'taxable' => 'bool',
                'values' => 'array',
                'values.baseCalculo' => 'numeric',
                'values.deducoes' => 'numeric',
                'values.descontoCondicionado' => 'numeric',
                'values.descontoIncondicionado' => 'numeric',
                'values.liquido' => 'numeric',
                'values.unitario' => 'numeric',
                'values.valorAproximadoTributos' => 'numeric',
                'iss' => 'array',
                'iss.tributavel' => 'bool',
                'iss.tipoTributacao' => 'string',
                'iss.exigibilidade' => 'bool',
                'iss.aliquota' => 'numeric',
                'iss.valor' => 'numeric',
                'iss.valorRetido' => 'numeric',
                'iss.processoSuspensao' => 'numeric',
                'pis' => 'array',
                'pis.baseCalculo' => 'numeric',
                'pis.aliquota' => 'numeric',
                'pis.valor' => 'numeric',
                'pis.cst' => 'int',
                'cofins' => 'array',
                'cofins.baseCalculo' => 'numeric',
                'cofins.aliquota' => 'numeric',
                'cofins.valor' => 'numeric',
                'cofins.cst' => 'int',
                'csll' => 'array',
                'csll.aliquota' => 'numeric',
                'csll.valor' => 'numeric',
                'inss' => 'array',
                'inss.aliquota' => 'numeric',
                'inss.valor' => 'numeric',
                'irrf' => 'array',
                'irrf.aliquota' => 'numeric',
                'irrf.valor' => 'numeric',
                'cpp' => 'array',
                'cpp.aliquota' => 'numeric',
                'cpp.valor' => 'numeric'
            ],
            default => []
        };

        return $validate;
    }
}
