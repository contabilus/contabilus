<?php

namespace App\Http\Requests;

use App\Rules\CpfCnpjRule;
use Illuminate\Foundation\Http\FormRequest;

class TakerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validated = match (strtolower($this->method())) {
            'post' => [
                'corporate_name' => 'required|max:255',
                'trading_name' => 'required|max:255',
                'cpf_cnpj' => ['required', new CpfCnpjRule],
                'state_registration' => 'required',
                'municipal_registration' => 'required',
                'email' => 'required|email',
                'public_agency' => 'nullable',
                'phone_prefix' => 'required|digits:2',
                'phone_number' => 'required|int',
                'address.zipcode' => 'required|max:10',
                'address.public_place' => 'required|max:125',
                'address.street' => 'required|max:255',
                'address.number' => 'required|int',
                'address.complement' => 'nullable',
                'address.neighborhood_type' => 'required',
                'address.neighborhood' => 'required',
                'address.city' => 'required',
                'address.state' => 'required|max:2',
                'address.country_code' => 'required|digits_between:1,3',
                'address.country' => 'required',
            ],
            'put' => [
                'corporate_name' => 'max:255',
                'trading_name' => 'max:255',
                'cpf_cnpj' => 'nullable|string',
                'state_registration' => 'nullable|string',
                'municipal_registration' => 'nullable|string',
                'email' => 'email',
                'public_agency' => 'nullable',
                'telephone.prefix' => 'digits:2',
                'telephone.phone' => 'max:10',
                'address.zipcode' => 'max:10',
                'address.public_place' => 'max:125',
                'address.street' => 'max:255',
                'address.number' => 'int',
                'address.complement' => 'nullable',
                'address.neighborhood_type' => 'nullable|string',
                'address.neighborhood' => 'nullable|string',
                'address.city' => 'nullable|string',
                'address.state' => 'max:2',
                'address.country_code' => 'digits_between:1,3',
                'address.country' => 'nullable|string',
            ],
            default => []
        };

        return $validated;
    }
}
