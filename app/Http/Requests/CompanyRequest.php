<?php

namespace App\Http\Requests;

use App\Enums\Companies\REGIME_ENUM;
use App\Enums\Companies\TAX_REGIME_ENUM;
use App\Rules\CNPJRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return match($this->method) {
            'post' => [
                'cnpj' => ['required','unique:companies,cnpj', new CNPJRule()],
                'company_name' => 'required|string',
                'sn_chooser' => 'required|boolean',
                'tax_regime' => 'required|integer|in:'.implode(',',REGIME_ENUM::TAX_REGIME),
                'special_tax_regime' => 'required|integer|in:'.implode(',',TAX_REGIME_ENUM::TAX_REGIME),
                'phone' => 'required|string',
                'email' => 'required|email',
                'address' => 'required|array',
                'address.street' => 'required|string',
                'address.street_type' => 'required|string',
                'address.complement' => 'required|string',
                'address.neighborhood' => 'required|string',
                'address.neighborhood_type' => 'required|string',
                'address.number' => 'required|string',
                'address.city' => 'required|string',
                'address.city_code' => 'required|string',
                'address.state' => 'required|string',
                'address.zip_code' => 'required|string',
            ],
            'put' => [
                'cnpj' => ['unique:companies,cnpj,'.$this->get('company'), new CNPJRule()],
                'company_name' => 'nullable|string',
                'sn_chooser' => 'nullable|boolean',
                'tax_regime' => 'nullable|integer|in:'.implode(',',REGIME_ENUM::TAX_REGIME),
                'special_tax_regime' => 'nullable|integer|in:'.implode(',',TAX_REGIME_ENUM::TAX_REGIME),
                'phone' => 'nullable|string',
                'email' => 'nullable|email',
                'address' => 'nullable|array',
                'address.street' => 'nullable|string',
                'address.street_type' => 'nullable|string',
                'address.complement' => 'nullable|string',
                'address.neighborhood' => 'nullable|string',
                'address.neighborhood_type' => 'nullable|string',
                'address.number' => 'nullable|string',
                'address.city' => 'nullable|string',
                'address.city_code' => 'nullable|string',
                'address.state' => 'nullable|string',
                'address.zip_code' => 'nullable|string',
            ]
        };

    }
}
