<?php

namespace App\Http\Resources\Company;

use App\Http\Resources\Address\AddressApiResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'cnpj' => $this['cpf_cnpj'],
            'company_name' => $this['razao_social'],
            'fantasy_name' => $this['nome'] ?? '',
            'cnaes' => $this['atividades'],
            'address' => new AddressApiResource($this['endereco']),
        ];
    }
}
