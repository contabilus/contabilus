<?php

namespace App\Http\Resources;

use App\Http\Resources\Address\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'cnae_id' => $this->cnae_id,
            'service_code' => $this->service_code,
            'service_details' => $this->service_details,
            'tax_code' => $this->tax_code,
            'city_code' => $this->city_code,
            'city' => $this->city,
            'unit' => $this->unit,
            'quantity' => $this->quantity,
            'others_tax' => $this->others_tax,
            'taxable' => $this->taxable,
            'values' => $this->values,
            'iss' => $this->iss,
            'pis' => $this->pis,
            'cofins' => $this->cofins,
            'csll' => $this->csll,
            'inss' => $this->inss,
            'irrf' => $this->irrf,
            'cpp' => $this->cpp
        ];
    }
}
