<?php

namespace App\Http\Resources\Address;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'zipcode' => $this->zipcode,
            'public_place' => $this->zipcode,
            'street' => $this->zipcode,
            'number' => $this->number,
            'complement' => $this->complement,
            'neighborhood_type' => $this->neighborhood_type,
            'neighborhood' => $this->neighborhood,
            'city' => $this->city,
            'state' => $this->state,
            'country_code' => $this->country_code,
            'country' => $this->country
        ];
    }
}
