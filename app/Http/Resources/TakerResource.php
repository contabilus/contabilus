<?php

namespace App\Http\Resources;

use App\Http\Resources\Address\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TakerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'corporate_name' => $this->corporate_name,
            'trading_name' => $this->trading_name,
            'cpf_cnpj' => $this->cpf_cnpj,
            'state_registration' => $this->state_registration,
            'municipal_registration' => $this->municipal_registration,
            'email' => $this->email,
            'public_agency' => $this->public_agency,
            'phone_prefix' => $this->phone_prefix,
            'phone_number' => $this->phone_number,
            'address' => new AddressResource($this->address)
        ];
    }
}
