<?php

namespace App\Http\Controllers\Api\Taker;

use App\Http\Controllers\Controller;
use App\Http\Requests\TakerRequest;
use App\Http\Resources\TakerResource;
use App\Models\Taker;

class TakerApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TakerResource::collection(Taker::orderBy('corporate_name', 'ASC')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTakerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TakerRequest $request)
    {
        $taker = Taker::create($request->safe()->except('address'));
        $taker->address()->create($request->safe()->only('address')['address']);

        return new TakerResource($taker);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Taker  $taker
     * @return \Illuminate\Http\Response
     */
    public function show(Taker $taker)
    {
        return new TakerResource($taker);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTakerRequest  $request
     * @param  \App\Models\Taker  $taker
     * @return \Illuminate\Http\Response
     */
    public function update(TakerRequest $request, Taker $taker)
    {
        $taker->update($request->safe()->except('address'));
        if ($request->get('address'))
            $taker->address()->update($request->safe()->only('address')['address']);

        return new TakerResource($taker);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Taker  $taker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Taker $taker)
    {
        $taker->delete();
    }
}
