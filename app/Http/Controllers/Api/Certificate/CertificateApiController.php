<?php

namespace App\Http\Controllers\Api\Certificate;

use App\Http\Controllers\Controller;
use App\Http\Requests\CertificateRequest;
use App\Http\Resources\CertificateResource;
use App\Models\Certificate;
use App\Services\NFE\NFServices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CertificateApiController extends Controller
{
    public function show($id)
    {
        try{
            $certificate = Certificate::firstWhere("api_id", $id);
            if(!$certificate) {
                $apiCert = (new NFServices('certificate'))->show(['id'=>$id]);
                $certificate = Certificate::create([
                    'expiration' => date('Y-m-d H:i:s', strtotime($apiCert['vencimento'])),
                    'api_id' => $apiCert['id'],
                ]);
            }
            return new CertificateResource($certificate);
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
    public function index()
    {
            /** TODO: Scope for company certificates */
            return CertificateResource::collection(Certificate::all());
    }
    public function store(CertificateRequest $request)
    {
        try
        {
            $response = (new NFServices('certificate'))->store([],['multipart'=> [['name'=>'arquivo', 'contents' => $request->file('arquivo'),'filename'=>$request->file('arquivo')->getClientOriginalName()],['name'=> 'senha', 'contents'=>  $request->senha]]]);
            $apiCert = (new NFServices('certificate'))->show(['id'=>$response['data']['id']]);
            $certificate = Certificate::create([
                'expiration' => date('Y-m-d H:i:s', strtotime($apiCert['vencimento'])),
                'api_id' => $apiCert['id'],
            ]);
            return new CertificateResource($certificate) ;
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
    public function update(CertificateRequest $request,$id)
    {
        try{
            $apiCert =(new NFServices('certificate'))->show(['id'=>$id],['multipart'=> [['name'=>'arquivo', 'contents' => $request->file('arquivo'),'filename'=>$request->file('arquivo')->getClientOriginalName()],['name'=> 'senha', 'contents'=>  $request->senha]]]);
            $cert = Certificate::firstWhere('api_id',$id);
            $cert->update([
                'expiration' => date('Y-m-d H:i:s', strtotime($apiCert['vencimento'])),
                'api_id' => $apiCert['id'],
            ]);
            return new CertificateResource($cert);
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
