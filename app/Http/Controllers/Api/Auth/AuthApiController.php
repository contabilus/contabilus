<?php

namespace App\Http\Controllers\Api\Auth;

use App\Actions\Fortify\CreateNewUser;
use App\Helpers\CodeHelper\CodeHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\RecoverToken;
use App\Models\User;
use App\Notifications\RecoverPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthApiController extends Controller
{
    public function login(AuthLoginRequest $request)
    {
        if(!auth()->attempt($request->validated())) {
            return $this->errorResponse(__('feedback.auth.failed'),Response::HTTP_UNAUTHORIZED);
        }
        $user = User::firstWhere('email', $request->get('email'));
        $token = $user->createToken('api_token')->plainTextToken;

        return $this->successResponse(['token'=>$token, 'user' => new UserResource($user)], __('feedback.auth.success'));
    }

    /** TODO: Revisar dados de entrega para cadastro. Criar regra de validação na request. */
    public function register(AuthRegisterRequest $request)
    {
        $user = (new CreateNewUser())->create($request->validated());
        $token = $user->createToken('api_token')->plainTextToken;
        return $this->successResponse(['token' => $token, 'user' => new UserResource($user)], __('feedback.auth.registered'));
    }

    public function forgotPassword(Request $request)
    {
        $request->validate([
            'email'=> 'required|exists:users,email'
        ]);

        $user = User::firstWhere('email', $request->get('email'));
        $token = CodeHelper::genereateRandomCode(6);
        RecoverToken::create([
            'email' => $request->get('email'),
            'code' => $token
        ]);

        $user->notify(new RecoverPassword($user,$token));

        return $this->successResponse([],__('feedback.auth.forget_password'));

    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'code'=> 'required|exists:recover_tokens,code',
            'password' => 'required|string|min:6'
        ]);

        $token = RecoverToken::firstWhere('code', $request->get('code'));
        $user = User::firstWhere('email', $token->email);
        $token->delete();
        $user->update([
            'password' => Hash::make($request->get('password'))
        ]);

        return $this->successResponse([], __('feedback.auth.reset_password'));

    }

    public function getMe()
    {
        return new UserResource(Auth::user());
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();
        return $this->successResponse([], __('feedback.auth.logout'));
    }
}
