<?php

namespace App\Http\Controllers\Api\NF;

use App\Http\Controllers\Controller;
use App\Services\NFE\NFServices;
use Illuminate\Http\Request;

class NFApiController extends Controller
{
    public function test()
    {
        try{
            return (new NFServices('certificate',['sandbox'=>true, 'Content-Type'=>'multipart/form-data']))->show(['id'=>'616b0fa6d7203c5e5e871dc8']);
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
