<?php

namespace App\Http\Controllers\Api\Company;

use App\Actions\Company\CreateCompany;
use App\Actions\Company\UpdateCompany;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Resources\Company\CompanyApiResource;
use App\Http\Resources\Company\CompanyResource;
use App\Models\Company;
use App\Services\NFE\NFServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class CompanyApiController extends Controller
{

    public function index()
    {
        return CompanyResource::collection(Company::fromUser()->get());
    }


    public function store(CompanyRequest $request)
    {
        DB::beginTransaction();
        try {
            $company = (new CreateCompany())->handle($request->validated());
            DB::commit();
            return $this->successResponse(new CompanyResource($company),__('feedback.general.created',['entity' => 'empresa']));
        }
        catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }


    public function show(Company $company):CompanyResource
    {
        return new CompanyResource($company);
    }

    public function findCnpj($cnpj)
    {
        $company = Company::firstWhere('cnpj', $cnpj);
        if(!$company) {
            $client = new NFServices('helpers');
            $data = $client->findCNPJ(['cnpj'=>$cnpj]);
            return new CompanyApiResource($data);
        }
        return new CompanyResource($company);
    }



    public function update(CompanyRequest $request, Company $company)
    {
        DB::beginTransaction();
        try {
            $company = (new UpdateCompany())->handle($request->validated(),$company);
            DB::commit();
            return $this->successResponse(new CompanyResource($company),__('feedback.general.updated',['entity' => 'empresa']));
        }
        catch (\Exception $e) {
            DB::rollBack();
            return $this->errorResponse($e->getMessage(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function destroy($id)
    {
        //
    }
}
