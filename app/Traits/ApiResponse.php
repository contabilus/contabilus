<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

trait ApiResponse
{
    public function successResponse($data , $message, $code = Response::HTTP_OK)
    {
        return response()->json(['status'=>'success','message'=>$message, 'data'=> $data],  $code);
    }
    public function errorResponse( $message, $code = Response::HTTP_BAD_REQUEST)
    {
        return response()->json(['status'=>'error','message'=>$message, 'data'=> []], $code);
    }
}
