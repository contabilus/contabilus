<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CpfCnpjRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (new CnpjFormatRule)->passes($attribute, $value) || (new CpfFormatRule)->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */

    public function message()
    {
        return __('validation.cpf_cnpj');
    }
}
