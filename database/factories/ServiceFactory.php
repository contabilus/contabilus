<?php

namespace Database\Factories;

use App\Models\Service;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'month' => $this->faker->month($max = 'now'),
            'year' => $this->faker->year($max = 'now'),
            'aliquot' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),
            'cnpj' => $this->faker->numerify('##############'), 
            'description' => $this->faker->randomNumber($nbDigits = NULL, $strict = false),
            'country' => $this->faker->country(),
            'state' => $this->faker->state(),
            'county' => $this->faker->cityPrefix(),
            'value' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'due_payment' => $this ->faker->date($format = 'd-m-Y', $max = 'now'),
        ];
    }
}
