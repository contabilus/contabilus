<?php

namespace Database\Factories;

use App\Models\Nf;
use App\Models\Cnae;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'soscial_reason' => $this->faker->name(),
            'certificate' => $this->faker->uuid(),
            'cnae' => $this->faker->ean8(),
            'nfs_id' => Nf::factory()->create('id')->id,
            'cnaes_id' => Cnae::factory()->create('id')->id,
        ];
    }
}
