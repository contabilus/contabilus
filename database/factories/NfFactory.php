<?php

namespace Database\Factories;

use App\Models\Nf;
use Illuminate\Database\Eloquent\Factories\Factory;

class NfFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Nf::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'note_number' => $this->faker->ean13(),
            'iss_retain' => $this->faker->boolean(),
            'reduction_value' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'unconditional_discounts' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL), 
            'conditioned_discounts' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'other_entrances' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'ir' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'inss' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'pis' => $this->faker-randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'cofins' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'csll' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'payment_type' => $this->faker->randomDigit(),
            'installments' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            
        ];
    }
}
