<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Jetstream\Features;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'cnpj' => $this->faker->numerify('##############'),
            'cpf' => $this->faker->numerify('###########'),
            'country' => $this->faker->country(),
            'state' => $this->faker->state(),
            'county' => $this->faker->cityPrefix(),
            'zip_code' => $this->faker->postcod(),
            'street' => $this->faker->streetName(),
            'number' => $this->faker->buildingNumber(),
            'neighberhood' => $this->faker->citySuffix(),
            'telephone' => $this->faker->phoneNumer(),
            'accountant' => $this->faker->fristNameMale(),
            'legal_nature' => $this->faker->fristNameMale(),
            'contract_duration' => $this ->faker->date($format = 'd-m-Y', $max = 'now'),
            'start_activity' => $this ->faker->date($format = 'd-m-Y', $max = 'now'),
            'representive_id' => Representative::factory()->create('id')->id,
            'role_id' => Role::factory()->create('id')->id,
            'company_id' => Company::factory()->create('user_id')->id,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    /**
     * Indicate that the user should have a personal team.
     *
     * @return $this
     */
    public function withPersonalTeam()
    {
        if (! Features::hasTeamFeatures()) {
            return $this->state([]);
        }

        return $this->has(
            Team::factory()
                ->state(function (array $attributes, User $user) {
                    return ['name' => $user->name.'\'s Team', 'user_id' => $user->id, 'personal_team' => true];
                }),
            'ownedTeams'
        );
    }
}
