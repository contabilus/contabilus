<?php

namespace Database\Factories;

use App\Models\Taker;
use Illuminate\Database\Eloquent\Factories\Factory;

class TakerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Taker::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'soscial_reason' => $this->faker->name(),
            'cpf' => $this->faker->numerify('###########'),
            'cnpj' => $this->faker->numerify('##############'), 
            'registration_municipality' => $this->faker->randomNumber($nbDigits = NULL, $strict = false),
            'country' => $this->faker->country(),
            'state' => $this->faker->state(),
            'county' => $this->faker->cityPrefix(),
            'zip_code' => $this->faker->postcod(),
            'street' => $this->faker->streetName(),
            'number' => $this->faker->buildingNumber(),
            'neighberhood' => $this->faker->citySuffix(),
            'telephone' => $this->faker->phoneNumer(),
            'email' => $this->faker->unique()->safeEmail(),
        ];
}
}