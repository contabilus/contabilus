<?php

namespace Database\Factories;

use App\Models\Cnae;
use Illuminate\Database\Eloquent\Factories\Factory;

class CnaeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cnae::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cnae' => $this->faker->ean8(),
            'title' => $this->faker->realText($maxNbChars = 20, $indexSize = 2),
            'annex' => $this->faker->realText($maxNbChars = 20, $indexSize = 2),
            
        ];
    }
}
