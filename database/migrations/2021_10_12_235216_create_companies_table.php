<?php
// namespace App\Enums\Companies;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('fantasy_name')->nullable();
            $table->string('cnpj');
            $table->boolean('sn_chooser')->default(true);
            $table->boolean('status')->default(true);
            $table->integer('tax_regime')->comment('Tipo de regimeTributário')->nullable();
            $table->integer('special_tax_regime')->comment('Regime especial Ex: MEI')->nullable();
            $table->string('phone');
            $table->string('email');
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
