<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_configs', function (Blueprint $table) {
            $table->id();
            $table->boolean('nfse_status')->default(true);
            $table->integer('nfse_type')->default(0);
            $table->boolean('nfse_production')->default(true);
            $table->json('nfse_rps')->nullable();
            $table->json('nfse_city_hall')->nullable()->comment('Caso não use certificado. { login, senha, receitaBruta,lei, dataInicio }');
            $table->string('nfse_email')->nullable();
            $table->boolean('nfe_status')->default(true);
            $table->integer('nfe_type')->default(0);
            $table->boolean('nfe_production')->default(true);
            $table->boolean('nfe_fecop')->default(false);
            $table->boolean('nfe_icms_share')->default(false);
            $table->boolean('nfe_dfe_active')->default(true);
            $table->string('nfe_email')->nullable();
            $table->string('nfe_number_initial')->nullable();
            $table->string('nfe_serial')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_configs');
    }
}
