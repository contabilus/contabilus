<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfs', function (Blueprint $table) {
            $table->id();
            $table->string('api_id');
            $table->string('integration_id')->comment('Número da nota ');

            $table->string('reduction_value');
            $table->string('unconditional_discounts');
            $table->string('conditioned_discounts');
            $table->string('other_entrances');
            $table->string('ir');
            $table->string('inss');
            $table->string('pis');
            $table->string('cofins');
            $table->string('csll');
            $table->string('payment_type');
            $table->string('installments');
            $table->foreignId('takers_id')->nullable();
            $table->foreignId('services_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfs');
    }
}
