<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Enums\NF\NFEnum;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('cnae_id');
            // $table->foreignId('user_id')->constrained('users');
            // $table->foreignId('cnae_id')->nullable()->constrained('cnaes');
            $table->string('service_code');
            $table->string('service_details')->comment('Discriminação');
            $table->string('tax_code')->comment('Código de tributação do município');
            $table->integer('city_code')->nullable()->comment('Código do IBGE');
            $table->string('city')->nullable();
            $table->string('unit')->nullable();
            $table->integer('quantity')->nullable();
            $table->json('values')->comment('Objeto com chaves: baseCalculo, deducoes, descontoCondicionado, descontoIncondicionado, liquido, unitario, valorAproximadoTributos');
            $table->json('iss')->comment('Objeto com chaves: tributavel, tipoTributacao, exigibilidade, aliquota, valor, valorRetido, processoSuspensao');
            $table->json('pis')->nullable()->comment('Objeto com chaves: baseCalculo, aliquota, valor , cst');
            $table->json('cofins')->nullable()->comment('Objeto com chaves: baseCalculo, aliquota, valor , cst');
            $table->json('csll')->nullable()->comment('Objeto com chaves:  aliquota, valor ');
            $table->json('inss')->nullable()->comment('Objeto com chaves:  aliquota, valor ');
            $table->json('irrf')->nullable()->comment('Objeto com chaves:  aliquota, valor ');
            $table->json('cpp')->nullable()->comment('Objeto com chaves:  aliquota, valor ');
            $table->float('others_tax')->nullable()->comment('Valor de outras retenções');
            $table->boolean('taxable')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
