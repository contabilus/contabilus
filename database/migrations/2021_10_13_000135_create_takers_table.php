<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('takers', function (Blueprint $table) {
            $table->id();
            $table->string('corporate_name');
            $table->string('trading_name');
            $table->string('cpf_cnpj');
            $table->string('state_registration');
            $table->string('municipal_registration');
            $table->string('email');
            $table->string('public_agency')->nullable();
            $table->integer('phone_prefix');
            $table->integer('phone_number');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('takers');
    }
}
