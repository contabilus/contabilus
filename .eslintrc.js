module.exports = {
    env: {
        browser: true,
        node: true,
    },
    extends: ['plugin:react/recommended', 'airbnb', 'prettier'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: ['react', 'import', 'prettier'],
    rules: {
        'no-plusplus': [2, { allowForLoopAfterthoughts: true }],
        'prettier/prettier': 0,
        'no-underscore-dangle': 0,
        'no-use-before-define': 0,
        'no-undef': 0,
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        'react/jsx-props-no-spreading': 0,
        'react/react-in-jsx-scope': 0,
    },
    parser: 'babel-eslint',
};
