<?php

return [
    'auth'=> [
        'success' => 'Login realizado com sucesso',
        'failed' => 'Email ou senha inválidos',
        'registered' => 'Usuário cadastrado com sucesso',
        'logout' => 'Usuário deslogado com sucesso',
        'forget_password' => 'Email de recuperação enviado',
        'reset_password' => 'Senha trocada com sucesso'
    ],
    'general' => [
        'created' => 'O registro de :entity foi criado com sucesso',
        'updated' => 'O registro de :entity foi atualizado com sucesso',
        'deleted' => 'O registro de :entity foi deletado com sucesso',
    ]
];
