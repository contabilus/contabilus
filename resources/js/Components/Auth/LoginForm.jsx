import { Inertia } from '@inertiajs/inertia';
import { Alert, Button, TextField } from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { usePage } from '@inertiajs/inertia-react';

const validationSchema = Yup.object().shape({
  email: Yup.string().email('Formato inválido').required('* Campo obrigatório'),
  password: Yup.string().required('* Campo obrigatório'),
});

const LoginForm = () => {
  const { props } = usePage();
  const formik = useFormik({
    initialValues: { email: '', password: '' },
    validationSchema,
    onSubmit: (values) => {
      Inertia.post('/login', values);
    },
  });

  return (
    <>
      {Object.keys(props.errors).length > 0 ? (
        <Alert severity='warning'>
          {Object.values(props.errors).map((v) => v)}
        </Alert>
      ) : null}
      <form
        onSubmit={formik.handleSubmit}
        className='flex flex-col items-center justify-between'
      >
        <TextField
          id='email'
          name='email'
          variant='outlined'
          label='Email'
          color='primary'
          onChange={formik.handleChange}
          error={!!formik.errors.email}
          helperText={formik.errors.email}
          size='small'
          margin='normal'
        />

        <TextField
          id='password'
          name='password'
          type='password'
          variant='outlined'
          label='Senha'
          color='primary'
          onChange={formik.handleChange}
          error={!!formik.errors.password}
          helperText={formik.errors.password}
          size='small'
          margin='normal'
        />

        <Button
          color='primary'
          margin='normal'
          variant='outlined'
          onClick={formik.submitForm}
        >
          Login
        </Button>
      </form>
    </>
  );
};

export default LoginForm;
