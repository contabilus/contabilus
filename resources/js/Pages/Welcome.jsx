import React from 'react';
import { Link, usePage } from '@inertiajs/inertia-react';
import MainLayout from '@/Layouts/MainLayout/MainLayout';

const Welcome = () => {
  const { props } = usePage();
  console.log(props);
  return (
    <Link method='post' color='primary' href='/logout' type='button'>
      Logout
    </Link>
  );
};

Welcome.layout = (page) => <MainLayout>{page}</MainLayout>;

export default Welcome;
