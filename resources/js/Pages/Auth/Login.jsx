import { Box } from '@mui/material';
import HeadlessLayout from '@/Layouts/HeadlessLayout/HeadlessLayout';
import LoginForm from '@/Components/Auth/LoginForm';

const Login = () => (
  <Box>
    <LoginForm />
  </Box>
);

Login.layout = (page) => <HeadlessLayout>{page}</HeadlessLayout>;

export default Login;
