import React from 'react';
import PropTypes from 'prop-types';
import { Container, Box } from '@mui/material';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import DescriptionIcon from '@mui/icons-material/Description';
import { useWindowSize } from 'react-use';
import { usePage } from '@inertiajs/inertia-react';


const links = [
    {
        title: 'Usuários',
        route: 'usuarios',
        icon: <PeopleAltIcon fontSize='large' />,
    },
    {
        title: 'Veículos',
        route: 'veiculos',
        icon: <DirectionsCarIcon fontSize='large' />,
    },
    {
        title: 'Contratos',
        route: 'contratos',
        icon: <DescriptionIcon fontSize='large' />,
    },
];

const MainLayout = ({ children, hasSidebar }) => {
    const { width } = useWindowSize();
    const { sidebarWidth } = usePage().props;
    const isDesktop = width >= 1024;

    return (
        <Box
            sx={
                hasSidebar && isDesktop
                    ? {
                        width: { sm: `calc(100% - ${sidebarWidth}px)` },
                        ml: { sm: `${sidebarWidth}px` },
                    }
                    : {}
            }
        >
            <div className='flex flex-col flex-wrap h-full w-full'>
                <div className='flex flex-row flex-wrap w-full flex-auto'>
                    <main className='w-full'>
                        <Container className='py-20'>
                            <div className='py-10'>{children}</div>
                        </Container>
                    </main>
                </div>
            </div>
        </Box>
    );
};

MainLayout.defaultProps = {
    hasSidebar: true,
};

MainLayout.propTypes = {
    children: PropTypes.node.isRequired,
    hasSidebar: PropTypes.bool,
};

export default MainLayout;
