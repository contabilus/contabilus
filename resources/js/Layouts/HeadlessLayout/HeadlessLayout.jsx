import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const MainLayout = ({ children }) => (
  <Box className='flex flex-row w-full h-full '>
    <div className='hidden sm:flex sm:flex-1 h-full w-0 sm:w-1/2 items-center justify-center '>
      img
    </div>
    <div className='flex flex-1 h-full w-full items-center justify-center  sm:w-1/2 '>
      {children}
    </div>
  </Box>
);

MainLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MainLayout;
