import React from 'react';
import { render } from 'react-dom';
import { createInertiaApp } from '@inertiajs/inertia-react';
import { InertiaProgress } from '@inertiajs/progress';
import { ThemeProvider } from '@mui/material';
import defaultTheme from './Themes/defaultTheme';

require('./bootstrap');

createInertiaApp({
    resolve: (name) => import(`./Pages/${name}`).then((module) => module.default),
    setup({ el, App, props }) {
        render(
            <ThemeProvider theme={defaultTheme}>
                <App {...props} />
            </ThemeProvider>,
            el,
        );
    },
});

InertiaProgress.init();
