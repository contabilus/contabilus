import { createTheme } from '@mui/material';


const defaultTheme = createTheme({
    typography: {
        htmlFontSize: 10,
        fontFamily: ['Roboto', 'Arial', 'sans-serif'].join(','),
        fontWeightLight: 300,
        fontWeightRegular: 400,
        fontWeightMedium: 500,
        fontWeightBold: 700,
    },
});

export default defaultTheme;
